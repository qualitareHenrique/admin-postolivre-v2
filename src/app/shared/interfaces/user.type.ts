export interface User {
    id: string;
    email: string;
    cnpj: string;
    social_reason: string;
    latest_login: string;
    phone_number: string;
}

export interface LoginResponse {
    access_token: string,
    expires_in: number,
    user: User
}