import { Injectable } from "@angular/core";
import { CanLoad, Route, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
import { LoginResponse } from '../interfaces/user.type';
import { NzNotificationService } from 'ng-zorro-antd';

@Injectable()
export class AuthGuard implements CanLoad, CanActivate {

    private user: LoginResponse;
    // private userLogged = false;

    constructor(private authService: AuthenticationService, private router: Router, private notification: NzNotificationService) {
        this.authService.currentUser.subscribe(user => this.user = user);
    }
    canActivate(): boolean |  Promise<boolean> {
        // console.log("Can Activate", this.user);
        if(!this.user) {
            this.notification.error("Usuário não autenticado", "Faça login para ter acesso");
            this.router.navigate(['/auth/login']);
        }
        
        return true;
    }

    canLoad(): boolean | Observable<boolean> | Promise<boolean> {
        return this.user ? true : false;
    }
}