import { SideNavInterface } from '../../interfaces/side-nav.type';
export const ROUTES: SideNavInterface[] = [
    {
        path: '/dashboard/home',
        title: 'Dashboard',
        iconType: 'nzIcon',
        iconTheme: 'outline',
        icon: 'dashboard',
        submenu: []
    },
    {
        path: '/users',
        title: 'Usuários',
        iconType: 'nzIcon',
        iconTheme: 'outline',
        icon: 'team',
        submenu: [
            // {
            //     path: '/posto',
            //     title: 'Posto',
            //     iconType: 'nzIcon',
            //     iconTheme: 'outline',
            //     icon: 'user',
            //     submenu: []
            // },
            // {
            //     path: '/distribuidora',
            //     title: 'Distribuidora',
            //     iconType: 'nzIcon',
            //     iconTheme: 'outline',
            //     icon: 'user',
            //     submenu: []
            // }
        ]
    },
    {
        path: '/auctions',
        title: 'Pedidos',
        iconType: 'nzIcon',
        iconTheme: 'outline',
        icon: 'bank',
        submenu: []
    },
    {
        path: '/units',
        title: 'Unidades',
        iconType: 'nzIcon',
        iconTheme: 'outline',
        icon: 'trademark-circle',
        submenu: []
    },
    {
        path: '/lotes',
        title: 'Lotes',
        iconType: 'nzIcon',
        iconTheme: 'outline',
        icon: 'partition',
        submenu: []
    },
    {
        path: '/fuels',
        title: 'Combustível',
        iconType: 'nzIcon',
        iconTheme: 'outline',
        icon: 'fire',
        submenu: []
    },
    {
        path: '/faq',
        title: 'FAQ',
        iconType: 'nzIcon',
        iconTheme: 'outline',
        icon: 'customer-service',
        submenu: []
    }
    
]    