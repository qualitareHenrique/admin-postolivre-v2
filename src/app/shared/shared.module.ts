import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { RouterModule } from "@angular/router";
import { NzIconModule } from 'ng-zorro-antd/icon';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ThemeConstantService } from './services/theme-constant.service';
import { SearchPipe } from './pipes/search.pipe';
import { SearchRecursivePipe } from './pipes/search-recursive.pipe';

@NgModule({
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        HttpClientJsonpModule,
        NzIconModule,
        PerfectScrollbarModule,
        SearchPipe,
        SearchRecursivePipe
    ],
    imports: [
        RouterModule,
        CommonModule,
        NzIconModule,
        PerfectScrollbarModule
    ],
    declarations: [
        SearchPipe,
        SearchRecursivePipe
    ],
    providers: [
        ThemeConstantService
    ]
})

export class SharedModule { }
