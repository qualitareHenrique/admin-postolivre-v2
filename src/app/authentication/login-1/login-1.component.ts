import { Component } from '@angular/core'
import { FormBuilder, FormGroup,  Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
    templateUrl: './login-1.component.html',
    styleUrls: ['./login-1.scss']
})

export class Login1Component {
    loginForm: FormGroup;

    constructor(private fb: FormBuilder, private authService: AuthenticationService, private router: Router) {}

    submitForm(): void {
        for (const i in this.loginForm.controls) {
            this.loginForm.controls[ i ].markAsDirty();
            this.loginForm.controls[ i ].updateValueAndValidity();
        }

        if(this.loginForm.valid) {
            this.authService.login(this.loginForm.get('username').value, this.loginForm.get('password').value).subscribe(() => {
                this.router.navigate(['/dashboard/home']);
            });
        }
    }

    ngOnInit(): void {
        this.loginForm = this.fb.group({
            username: [ environment.production ? '' : 'admin@postolivre.com', [ Validators.required ] ],
            password: [ environment.production? '': 'secret', [ Validators.required ] ]
        });
    }
}    