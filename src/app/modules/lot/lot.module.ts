import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LotRoutingModule } from './lot-routing.module';
import { LotListComponent } from './lot-list/lot-list.component';
import { NzCardModule, NzLayoutModule, NzTableModule, NzDescriptionsModule, NzBadgeModule, NzInputModule, NzFormModule, NzDatePickerModule, NzButtonModule, NzIconModule, NzSelectModule, NzSpinModule } from 'ng-zorro-antd';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

const antdModules = [
  NzCardModule,
  NzLayoutModule,
  NzTableModule,
  NzDescriptionsModule,
  NzBadgeModule,
  NzInputModule,
  NzFormModule,
  NzDatePickerModule,
  NzButtonModule,
  NzIconModule,
  NzSelectModule,
  NzSpinModule
];

@NgModule({
  declarations: [
    LotListComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LotRoutingModule,
    ...antdModules
  ],
})
export class LotModule { }
