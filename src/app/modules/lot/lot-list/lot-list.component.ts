import { Component, OnInit } from '@angular/core';

import { LotService } from '../../../shared/services/api/lot.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FuelService } from 'src/app/shared/services/api/fuel.service';
import { finalize, delay } from 'rxjs/operators';
import {AuctionService} from "../../../shared/services/api/auction.service";

@Component({
  selector: 'app-lot-list',
  templateUrl: './lot-list.component.html',
  styleUrls: ['./lot-list.component.scss']
})
export class LotListComponent implements OnInit {
  search: any = '';
  searchFields: any = '';
  expandSet = new Set<number>();

  onExpandChange(id: number, checked: boolean): void {
    if (checked) {
      this.expandSet.add(id);
    } else {
      this.expandSet.delete(id);
    }
  }

  listOfColumn = [
    {
      title: 'Id',
      sortFn: (a, b) => a.id.localeCompare(b.id),
      sortOrder: null,
    },
    {
      title: 'Combustível',
      sortFn: (a, b) => a.fuel.name.localeCompare(b.fuel.name),
      sortOrder: null,
    },
    {
      title: 'Ponto de Retirada',
      sortFn: (a, b) => a.port.name.localeCompare(b.port.name),
      sortOrder: null,
    },
    {
      title: 'Total de Combustivel',
      sortFn: (a, b) => this.totalFuel(a) - this.totalFuel(b),
      sortOrder: null,
    },
    {
      title: 'Data de criação',
      sortFn: (a, b) => a.created_at.localeCompare(b.created_at),
    }
  ];
  listOfData: Array<any> = [];

  searchForm: FormGroup;
  fuelList: any[];
  isLoading: boolean = false;

  constructor(
      private lotService: LotService,
      private fb: FormBuilder,
      private fuelService: FuelService,
      private auctionService: AuctionService) {
    this.searchForm = this.fb.group({
      "fuel": this.fb.control(""),
      "range": this.fb.control(""),
      "station": this.fb.control("")
    })
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.lotService.read({ endpoint: 'lot', includes: 'fuel,port,auctions.station,auctions.uploads' })
    .pipe(
      finalize(() => this.isLoading = false)
    )
    .subscribe((data: any) => {
      data.lots.sort(function(a, b) { 
        return b.identifier - a.identifier;
      });

      this.listOfData = data.lots
    });

    this.fuelService.read({ endpoint: 'fuel' })
                    .subscribe((data: any) => this.fuelList = data.fuels);
  }

  totalFuel(lot) {
    return lot.auctions.auctions.reduce((total, auction) => {
      return total + auction.fuel_amount
    }, 0);
  }

  submitSearchForm() {
    this.isLoading = true;
    this.search = [];
    this.searchFields = '';
    const { station, fuel, range } = this.searchForm.value;

    if(fuel) {
      this.search.push(`fuel.id:${fuel}`);
      this.searchFields = 'fuel.id:='
    }

    if(station) {
      this.search.push(`auctions.station.user.social_reason:${station}`);
      this.searchFields += 'auctions.station.user.social_reason:like'
    }

    if(range && range.length) {
      this.search.push(`created_at:${new Date(range[0]).toISOString().slice(0,10)}/${new Date(range[1]).toISOString().slice(0,10)}`);
      this.searchFields += `created_at:between`
    }

    this.lotService.read({ 
      endpoint: 'lot', 
      includes: 'fuel,port,auctions.station,auctions.uploads',
      search: this.search.join(),
      searchFields: this.searchFields,
      searchJoin: 'and'
    })
    .pipe(delay(1500), finalize(() => this.isLoading = false))
    .subscribe((data: any) => {
      data.lots.sort(function(a, b) { 
        return b.identifier - a.identifier;
      });

      this.listOfData = data.lots;

    });
  }

  resetForm() {
    this.searchForm.reset();
    this.search = '';
    this.searchFields = '';
  }

  uploadFile(event, auction) {
    let formData = new FormData();
    formData.append('file', event.target.files[0]);

    this.auctionService.upload({
      endpoint: `auctions/${auction.id}/upload`,
    }, formData).subscribe((data: any) => {
      auction.uploads.uploads.push(data);
    });
  }

}
