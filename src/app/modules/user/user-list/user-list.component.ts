import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { UserService } from 'src/app/shared/services/api/user.service';
import { TableService } from 'src/app/shared/services/table.service';
import { delay, finalize } from 'rxjs/operators';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RoleService } from 'src/app/shared/services/api/role.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { UserUpdateComponent } from '../user-update/user-update.component';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  listOfColumn = [
    {
      title: 'Id'
    },
    {
      title: 'Email',
      compare: (a, b) => a.email - b.email,
      // priority: 3
    },
    {
      title: 'Razão Social',
      compare: (a, b) => a.social_reason - b.social_reason,
      // priority: 2
    },
    {
      title: 'Cnpj',
      compare: (a, b) => a.cnpj - b.cnpj
    },
    {
      title: 'Inscrição Estadual',
      compare: (a, b) => a.state_registration - b.state_registration,
    },
    {
      title: 'Telefone',
      compare: (a, b) => a.phone_number - b.phone_number,
    },
    {
      title: 'Status',
    },
    {
      title: 'Último login'
    },
    {
      title: 'Email verificado em'
    },
    {
      title: 'Tipo'
    },
    {
      title: 'Perfil'
    },
    {
      title: 'Ações'
    }
  ];
  listOfData: Array<any> = [];
  usersList: Array<any> = [];
  roleList: Array<any> = [];
  searchInput: string;
  isLoading: boolean = false;
  searchForm: FormGroup;
  private _search: any = '';
  private _searchFields: any = '';

  constructor(
    private userService: UserService,
    private authService: AuthenticationService,
    private tableSvc: TableService,
    private fb: FormBuilder,
    private roleService: RoleService,
    private alertService: NzNotificationService,
    private nzModalService: NzModalService,
    private viewContainerRef: ViewContainerRef
  ) {
    this.searchForm = this.fb.group({
      role: this.fb.control("")
    });
  }

  search() {
    this.listOfData = this.tableSvc.search(this.searchInput, this.usersList)
  }

  /**
   * Verifica se é possível suspender um usuário. 
   * Só pode ser suspenso quando não é o próprio usuário executando a ação e quando não está suspenso.
   * @param {*} item
   * @returns
   * @memberof UserListComponent
   */
  canSuspendeUser(item) {
    return this.authService.currentUserValue.user.id !== item.id && !item.deleted_at;
  }

  ngOnInit(): void {
    this.loadData();
    this.roleService.read({ endpoint: 'roles' }).subscribe((roles: any) => {

      this.roleList = roles.roles.map((role) => {
        const { name, ...rest } = role;
        let roleFormatted = {};
        switch (name) {
          case 'admin':
            roleFormatted = { name: 'Admin', ...rest };
            break;
          case 'gas_station':
            roleFormatted = { name: 'Posto', ...rest };
            break;
          case 'distributor':
            roleFormatted = { name: 'Distribuidor', ...rest };
            break;
          default:
            break;
        }
        return roleFormatted;
      });
    });
  }

  suspender(user) {
    this.userService.delete({ endpoint: 'users', id: user.id }).subscribe(() => {
      this.alertService.success("Sucesso!", "Usuário suspenso com sucesso!");
      this.loadData();
    });
  }

  reativar(user) {
    this.userService.restore({ endpoint: 'users', id: user.id }).subscribe(() => {
      this.alertService.success("Sucesso!", "Usuário reativado com sucesso!");
      this.loadData();
    });
  }

  loadData() {
    this.isLoading = true;
    this.userService.read({ endpoint: 'users', includes: 'distributor.bank_account,station,roles.permissions,address' })
      .pipe(delay(1000), finalize(() => this.isLoading = false))
      .subscribe((users: any) => {
        this.listOfData = users.users
        this.usersList = users.users
      })
  }

  submitSearchForm() {
    this._search = '';
    this._searchFields = '';
    this.isLoading = true;
    const { role } = this.searchForm.value;
    if (role) {
      this._search = `roles.id:${role}`;
      this._searchFields = `roles.id:=`;
    }

    this.userService.read({
      endpoint: 'users',
      includes: 'distributor.bank_account,station,roles.permissions,address',
      search: this._search,
      searchFields: this._searchFields
    })
      .pipe(delay(1000), finalize(() => this.isLoading = false))
      .subscribe((users: any) => {
        this.listOfData = users.users
        this.usersList = users.users
      })

  }

  resetForm() {
    this.searchForm.reset();
    this.loadData();
  }

  showModalUpdate(usuario) {
    console.log(usuario);
    const modal = this.nzModalService.create({
      nzTitle: `Atualizar Usuário - ${usuario.social_reason}`,
      nzContent: UserUpdateComponent,
      nzViewContainerRef: this.viewContainerRef,
      nzComponentParams: {
        user: usuario
      },
      nzOnOk: () => new Promise(resolve => setTimeout(resolve, 1000)),
      nzFooter: [
        {
          label: 'Salvar',
          type: 'primary',
          loading: componentInstance => componentInstance.isLoading,
          onClick: componentInstance => {
            componentInstance.submitForm()
          }
        }
      ]
    })

    modal.afterClose.subscribe(result => {
      if (result) {
        this.loadData()
      }
    });
  }
}
