import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzCardModule, NzLayoutModule, NzTableModule, NzDescriptionsModule, NzBadgeModule, NzTagModule, NzDropDownModule, NzIconModule, NzInputModule, NzButtonModule, NzSpinModule, NzFormModule, NzSelectModule, NzModalService, NzTabsModule } from 'ng-zorro-antd';
import { UserListComponent } from './user-list/user-list.component';
import { UserRoutingModule } from './user-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserUpdateComponent } from './user-update/user-update.component';
import { NgxMaskModule } from 'ngx-mask';

const antdModules = [
  NzCardModule,
  NzLayoutModule,
  NzTableModule,
  NzDescriptionsModule,
  NzBadgeModule,
  NzTagModule,
  NzFormModule,
  NzDropDownModule,
  NzIconModule,
  NzInputModule,
  NzButtonModule,
  NzSpinModule,
  NzSelectModule,
  NzTabsModule
];

@NgModule({
  declarations: [
    UserListComponent,
    UserUpdateComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UserRoutingModule,
    NgxMaskModule.forRoot(),
    ...antdModules
  ],
  providers: [
    NzModalService
  ]
})
export class UserModule { }
