import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqRoutingModule } from './faq-routing.module';
import { FaqListComponent } from './faq-list/faq-list.component';
import { NzSpinModule, NzTableModule, NzFormModule, NzLayoutModule, NzCardModule, NzDescriptionsModule, NzSelectModule, NzButtonModule, NzDatePickerModule, NzIconModule, NzModalModule, NzPopconfirmModule, NzInputModule, NzToolTipModule } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FaqUpdateComponent } from './faq-update/faq-update.component';

const antdModules = [
  NzSpinModule,
  NzTableModule,
  NzFormModule,
  NzLayoutModule,
  NzCardModule,
  NzDescriptionsModule,
  NzFormModule,
  NzInputModule,
  NzSelectModule,
  NzButtonModule,
  NzDatePickerModule,
  NzIconModule,
  NzToolTipModule,
  NzModalModule,
  NzPopconfirmModule
];

@NgModule({
  declarations: [FaqListComponent, FaqUpdateComponent],
  imports: [
    CommonModule,
    FaqRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ...antdModules
  ]
})
export class FaqModule { }
