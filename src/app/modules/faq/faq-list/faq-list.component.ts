import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { delay, finalize } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { QuestionService } from 'src/app/shared/services/api/question.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { FaqUpdateComponent } from '../faq-update/faq-update.component';

@Component({
  selector: 'app-faq-list',
  templateUrl: './faq-list.component.html',
  styleUrls: ['./faq-list.component.css']
})
export class FaqListComponent implements OnInit {

  isVisible: boolean = false;
  search: any = '';
  searchFields: any = '';
  expandSet = new Set<number>();

  onExpandChange(id: number, checked: boolean): void {
    if (checked) {
      this.expandSet.add(id);
    } else {
      this.expandSet.delete(id);
    }
  }

  listOfColumn = [
    {
      title: 'Id',
      sortFn: (a, b) => a.id.localeCompare(b.id),
      sortOrder: null,
    },
    {
      title: 'Question',
      sortFn: (a, b) => a.question.localeCompare(b.question),
      sortOrder: null,
    },
    {
      title: 'Qtd Respostas',
      // sortFn: (a, b) => a
      sortOrder: null
    },
    {
      title: 'Data de criação',
      sortFn: (a, b) => a.created_at.localeCompare(b.created_at),
    },
    { title: '' }
  ];
  listOfData: Array<any> = [];

  searchForm: FormGroup;
  formCreate: FormGroup;
  fuelList: any[];
  isLoading: boolean = false;
  autoTips: Record<string, Record<string, string>> = {
    en: {
      required: 'Campo obrigatório',
      email: 'Email inválido'
    }
  };

  constructor(
    private questionService: QuestionService,
    private fb: FormBuilder,
    private modalService: NzModalService,
    private viewContainerRef: ViewContainerRef,
    private alertService: NzNotificationService
  ) {
    // this.searchForm = this.fb.group({
    //   "fuel": this.fb.control(""),
    //   "range": this.fb.control(""),
    //   "station": this.fb.control("")
    // })
    this.formCreate = this.fb.group({
      "question": this.fb.control(null, Validators.required),
      "answers": this.fb.array([], Validators.required)
    });

    this.addAnswer();
  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(): void {
    this.isLoading = true;
    this.questionService.read({ endpoint: 'questions', includes: 'answers' })
      .pipe(
        finalize(() => this.isLoading = false)
      )
      .subscribe((data: any) => this.listOfData = data.questions);
  }

  submitSearchForm() {

  }

  resetForm() {
    this.searchForm.reset();
    this.search = '';
    this.searchFields = '';
  }

  showModalUpdate(item) {
    console.log(item);
    const modal = this.modalService.create({
      nzTitle: `Atualizar Pergunta - ${item.question}`,
      nzContent: FaqUpdateComponent,
      nzViewContainerRef: this.viewContainerRef,
      nzComponentParams: {
        faq: item
      },
      nzOnOk: () => new Promise(resolve => setTimeout(resolve, 1000)),
      nzFooter: [
        {
          label: 'Salvar',
          type: 'primary',
          loading: componentInstance => componentInstance.isLoading,
          onClick: componentInstance => {
            componentInstance.submitForm()
          }
        }
      ]
    })

    modal.afterClose.subscribe(result => {
      if (result) {
        this.loadData()
      }
    });

  }

  delete(question: any) {
    this.questionService.delete({ endpoint: 'questions', id: question.id }).subscribe(() => this.loadData());
  }

  toggleModal() {
    this.isVisible = !this.isVisible;
  }

  handleOk(): void {
    for (const i in this.formCreate.controls) {
      this.formCreate.controls[i].markAsDirty();
      this.formCreate.controls[i].updateValueAndValidity();
    }
    if (this.formCreate.valid) {
      this.questionService.create({ endpoint: 'questions' }, this.formCreate.value).subscribe(() => {
        this.isVisible = false
        this.formCreate.reset();
        this.alertService.success("Sucesso", "Faq cadastrado com sucesso!");
        this.loadData()
      })
    }
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  removeField(index: number, e: MouseEvent): void {
    e.preventDefault();

    let respostas = this.formCreate.get('answers') as FormArray;
    if (respostas.length > 1) {
      respostas.removeAt(index);
    }
  }

  createAnswerGroup(): FormGroup {
    return this.fb.group({
      "answer": this.fb.control(null, Validators.required)
    });
  }

  addAnswer() {
    (this.formCreate.get('answers') as FormArray).push(this.createAnswerGroup());
  }

}
