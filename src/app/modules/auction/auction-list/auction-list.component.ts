import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { TableService } from 'src/app/shared/services/table.service';
import { AuctionService } from 'src/app/shared/services/api/auction.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { finalize, delay } from 'rxjs/operators';
import { FuelService } from 'src/app/shared/services/api/fuel.service';
import { PortService } from 'src/app/shared/services/api/port.service';
import { NzModalService, NzModalRef, NzNotificationService } from 'ng-zorro-antd';
import { AuctionUpdateComponent } from '../auction-update/auction-update.component';

interface DataItem {
  id: string;
  name: string;
  freight_type: string;
  fuel_amount: number;
  port: any;
  fuel: any;
  payment_validated_at: any | null;
  created_at: string;
}

@Component({
  selector: 'app-auction-list',
  templateUrl: './auction-list.component.html',
  styleUrls: ['./auction-list.component.css']
})
export class AuctionListComponent implements OnInit {

  allChecked: boolean = false;
  indeterminate: boolean = false;
  displayData = [];
  searchInput: string;
  searchForm: FormGroup;
  fuelList: any[];
  portList: any[];
  search: any = '';
  searchFields: any = '';
  isLoading: boolean = false;

  orderColumn = [
    {
      title: 'ID',
      compare: (a: DataItem, b: DataItem) => a.id.localeCompare(b.id),
    },
    {
      title: 'Tipo Frete',
      compare: (a: DataItem, b: DataItem) => a.freight_type.localeCompare(b.freight_type)
    },
    {
      title: 'Ponto de Retirada',
      compare: (a: DataItem, b: DataItem) => a.port?.name.localeCompare(b.port?.name)
    },
    {
      title: 'Status',
      // compare: (a: DataItem, b: DataItem) => a.port?.name.localeCompare(b.port?.name)
    },
    {
      title: 'Comprovante de Pgto',
      // compare: (a: DataItem, b: DataItem) => a.port?.name.localeCompare(b.port?.name)
    },
    {
      title: 'Pedido',
      compare: (a: DataItem, b: DataItem) => a.fuel?.name.localeCompare(b.fuel?.name)
    },
    {
      title: 'Quantidade (L)',
      compare: (a: DataItem, b: DataItem) => a.fuel_amount - b.fuel_amount
    },
    {
      title: 'Data de criação',
      compare: (a: DataItem, b: DataItem) => a.created_at.localeCompare(b.created_at)
    },
    {
      title: ''
    }
  ]

  auctionList = [];

  constructor(
    private tableSvc: TableService,
    private fb: FormBuilder,
    private auctionService: AuctionService,
    private alertService: NzNotificationService,
    private nzModalService: NzModalService,
    private viewContainerRef: ViewContainerRef,
    private portService: PortService,
    private fuelService: FuelService) {
    this.searchForm = this.fb.group({
      "fuel": this.fb.control(""),
      "range": this.fb.control(""),
      "freight_type": this.fb.control(""),
      "port": this.fb.control("")
    })
  }

  /* search() {
    this.displayData = this.tableSvc.search(this.searchInput, this.auctionList)
  } */

  delete(item) {
    this.auctionService.delete({ endpoint: 'auctions', id: item.id }).subscribe(() => {
      this.alertService.success("Sucesso!", "Pedido removido com sucesso!");
      this.loadData()
    });
  }

  ngOnInit() { 
    this.auctionService.read({ endpoint: 'auctions', includes: 'lot,port.unit,fuel,station' }).subscribe((data: any) => {
      console.log("return of get on init");
      console.log(data.auctions);
      /* data.auctions.sort(function(a, b) { 
        return b.identifier - a.identifier;
      }); */
      this.displayData = data.auctions
      this.auctionList = data.auctions
    });
    this.fuelService.read({ endpoint: 'fuel' }).subscribe((data: any) => this.fuelList = data.fuels);
    this.portService.read({ endpoint: 'ports' }).subscribe((data: any) => this.portList = data.ports);

  }

  submitSearchForm() {
    this.isLoading = true;
    this.search = '';
    this.searchFields = '';
    const { freight_type, port, fuel, range } = this.searchForm.value;
    if(freight_type) {
      this.search = `freight_type:${freight_type}`;
      this.searchFields = 'freight_type:='
    }
    if(port) {
      this.search += `port.id:${port}`;
      this.searchFields += 'port.id:='

    }
    if(fuel) {
      console.log("adding fuel being called");
      this.search += `fuel.id:${fuel}`;
      this.searchFields += 'fuel.id:='
    }
    if(range && range.length) {

      this.search += `created_at:${new Date(range[0]).toISOString().slice(0,10)},${new Date(range[1]).toISOString().slice(0,10)}`;
      this.searchFields += `created_at:between`
    }

    /* if(range && range.length) {

      this.search += `created_at:${new Date(range[0]).toISOString().slice(0,10)},${new Date(range[1]).toISOString().slice(0,10)}`;
      this.searchFields += `created_at:between`
    } */
    /* console.log(this.search); */
    // return
    this.auctionService.read({
      endpoint: 'auctions',
      includes: 'lot,port.unit,fuel,station',
      search: this.search,
      searchFields: this.searchFields,
      searchJoin: 'and'
    })
    .pipe(delay(1500), finalize(() => this.isLoading = false))
    .subscribe((data: any) => {
      console.log(data.auctions);
      /* data.auctions.sort(function(a, b) { 
        return b.identifier - a.identifier;
      }); */

      this.displayData = data.auctions
      this.auctionList = data.auctions
    });
  }

  resetForm() {
    this.searchForm.reset();
    this.search = '';
    this.searchFields = '';
  }

  /* showModalUpdate(item) {
    console.log("item being parsed to the modal");
    console.log(item);
    const modal = this.nzModalService.create({
      nzTitle: `Atualizar Leilões - ${item.identifier}`,
      nzContent: AuctionUpdateComponent,
      nzViewContainerRef: this.viewContainerRef,
      nzComponentParams: {
        auction: item
      },
      nzOnOk: () => new Promise(resolve => setTimeout(resolve, 1000)),
      nzFooter: [
        {
          label: 'Salvar',
          type: 'primary',
          loading: componentInstance => componentInstance.isLoading,
          onClick: componentInstance => {
            componentInstance.submitForm()
          }
        }
      ]
    })

    modal.afterClose.subscribe(result => {
      if(result) {
        this.loadData()
      }
    });

  } */

  loadData() {
    this.isLoading = true;
    this.auctionService.read({ endpoint: 'auctions', includes: 'lot,port.unit,fuel,station' })
    .pipe(delay(1000), finalize(() => this.isLoading = false))
    .subscribe((data: any) => {
      console.log(data.auctions)
      /* data.auctions.sort(function(a, b) { 
        return b.identifier - a.identifier;
      }); */

      this.displayData = data.auctions
      this.auctionList = data.auctions
    });
  }

  showModalUpdate(item) {
    console.log(item); 
    const modal = this.nzModalService.create({
      nzTitle: `Atualizar Pedido - ${item.id}`,
      nzContent: AuctionUpdateComponent,
      nzViewContainerRef: this.viewContainerRef,
      nzComponentParams: {
        auction: item
      },
      nzOnOk: () => new Promise(resolve => setTimeout(resolve, 1000)),
      nzFooter: [
        {
          label: 'Salvar',
          type: 'primary',
          loading: componentInstance => componentInstance.isLoading,
          onClick: componentInstance => {
            // componentInstance.submitForm()
          }
        }
      ]
    })

    modal.afterClose.subscribe(result => {
      if(result) {
        this.loadData()
      }
    });
  }

}
