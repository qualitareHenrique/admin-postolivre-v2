import { AuctionService } from 'src/app/shared/services/api/auction.service';
import { AuctionUpdateComponent } from './auction-update/auction-update.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuctionRoutingModule } from './auction-routing.module';
import { NzLayoutModule, NzCardModule, NzTableModule, NzBadgeModule, NzAvatarModule, NzSelectModule, NzInputModule, NzButtonModule, NzIconModule, NzPopconfirmModule, NzModalModule, NzSpinModule, NzFormModule, NzDescriptionsModule, NzDatePickerModule } from 'ng-zorro-antd';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuctionListComponent } from './auction-list/auction-list.component';
import { NgxMaskModule } from 'ngx-mask';

const antdModules = [
  NzLayoutModule,
  NzCardModule,
  NzTableModule,
  NzDescriptionsModule,
  NzBadgeModule,
  NzAvatarModule,
  NzSelectModule,
  NzInputModule,
  NzButtonModule,
  NzIconModule,
  NzFormModule,
  NzDatePickerModule,
  NzPopconfirmModule,
  NzModalModule,
  NzSpinModule,
  NzPopconfirmModule,
  NzModalModule,
  NzSpinModule,
  NzFormModule
];

@NgModule({
  declarations: [AuctionListComponent, AuctionUpdateComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AuctionRoutingModule,
    ...antdModules,
    NgxMaskModule.forRoot()
  ],
  providers: [AuctionService]

})
export class AuctionModule { }
