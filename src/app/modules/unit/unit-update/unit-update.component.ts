import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UnitService } from 'src/app/shared/services/api/unit.service';
import { delay, finalize } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-unit-update',
  templateUrl: './unit-update.component.html',
  styleUrls: ['./unit-update.component.css']
})
export class UnitUpdateComponent implements OnInit {
  
  form: FormGroup;
  @Input() unit: any;
  isLoading: boolean = false;

  autoTips: Record<string, Record<string, string>> = {
    en: {
      required: 'Campo obrigatório',
      email: 'Email inválido'
    }
  };

  constructor(private fb: FormBuilder, private unitService: UnitService, private modal: NzModalRef) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      "state_abbreviation": this.fb.control(this.unit.state_abbreviation, [Validators.required]),
      "email": this.fb.control(this.unit.email, [Validators.required, Validators.email]),
      "cpf_or_cnpj": this.fb.control(this.unit.cpf_or_cnpj, [Validators.required]),
      "name": this.fb.control(this.unit.name, [Validators.required]),
      "phone": this.fb.control(this.unit.phone)
    })
  }

  submitForm(): void {
    this.isLoading = true;
    this.unitService.update({ endpoint: 'units', id: this.unit.id }, this.form.value)
    .pipe(delay(1000), finalize(() => this.isLoading = false))
    .subscribe((result) => this.destroyModal(result));
  }

  destroyModal(data): void {
    this.modal.destroy(data);
  }

}
