import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FuelListComponent } from './fuel-list/fuel-list.component';

const routes: Routes = [
  { path: '', component: FuelListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FuelRoutingModule { }
